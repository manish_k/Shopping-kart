module.exports = function Kart(oldkart) {
    this.items = oldkart.items || {};
    this.totalQty = oldkart.totalQty || 0;
    this.totalPrice = oldkart.totalPrice || 0; 

    this.add = function(item, id) {
        var storedItem = this.items[id];
        if(!storedItem) {
            storedItem = this.items[id] = {item: 0, qty: 0, price: 0};
        }
        storedItem.qty++;
        storedItem.price = storedItem.item.price * storedItem.qty;
        this.totalQty++;
        this.totalPrice = storedItem.item.price;       
    }
    this.generateArray = function() {
            var arr = [];
            for(var id in this.items) {
                arr.push(this.items[id]);
            }
            return arr;
    }
}