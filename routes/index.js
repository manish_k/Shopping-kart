var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var passport = require('passport');

var Product = require('../models/product');
var Kart = require('../models/kart');

var csrfToken = csrf();
router.use(csrfToken)

/* GET home page. */
router.get('/', function(req, res, next) {
  var products = Product.find(function(err, docs) {
    res.render('shop/index', { products: docs });
  });  
});

router.get('/add-to-kart/:id', function(req, res, next) {
  var productId = req.params.id;
  var kart = new Kart(req.session.Kart? req.session.Kart: {items: {}});

  Product.findById(productId, function(err, product) {
    if (err) return res.redirect('/');
    kart.add(product, product.id)
    req.session.kart = kart;
    console.log(req.session.kart);
    res.redirect('/');
  })
})


module.exports = router;
