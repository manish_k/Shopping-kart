var mongoose = require('mongoose');
var Product = require('../models/product');

mongoose.connect('mongodb://127.0.0.1:27017/shopping');

var db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Connected!")
})

var products = [ 
    new Product({
        imagePath: 'https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
        title: 'Rose',
        description: 'A flower',
        price: 100
    }),
     
    new Product({
        imagePath: 'http://blog.uny.ac.id/kuswari/files/2009/12/animal_1011.jpg',
        title: 'puppy',
        description: "Welcome to Animal Mad! This website is for all those who love animals! I've only just started this site so there is'nt much in it. In a matter ",
        price: 1000
    }),
    ]

products.forEach(product => {
    product.save(function(err, msg) {
        if(err) throw err;
        console.log("data inserted :"+msg)
    })
});
mongoose.disconnect;