var mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/shopping');

var db = mongoose.connection;
db.once('open', function() {
    console.log("Connected!");
});
 
var product = mongoose.model('Product');
var items = {}

product.find({}, function(err, results) {
    if (err) throw err;
    items = results;
    items.forEach(item => {
        console.log(item);
    });
})